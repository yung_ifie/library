import os

DEBUG = False
SECURITY_PASSWORD_SALT = os.environ["SECURITY_PASSWORD_SALT"]
SECRET_KEY = os.environ["SECRET_KEY"]
MAIL_SERVER = 'smtp.gmail.com'
MAIL_PORT = 465
MAIL_USERNAME = os.environ["APP_MAIL_USERNAME"]
MAIL_PASSWORD = os.environ["APP_MAIL_PASSWORD"]
MAIL_USE_TLS = False
MAIL_USE_SSL = True
# mail accounts
MAIL_DEFAULT_SENDER = "ife@ifeslibrary.com"
MAILGUN_API_KEY = os.environ["MAILGUN_API_KEY"]
MAILGUN_FROM = os.environ["MAILGUN_FROM"]
MAILGUN_URL = os.environ["MAILGUN_URL"]

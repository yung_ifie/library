from functools import wraps
from threading import Thread

from flask import flash, redirect, url_for

from src.models.users.user import User


def async(func):
    def wrapper(*args, **kwargs):
        thr = Thread(target=func, args=args, kwargs=kwargs)
        thr.start()

    return wrapper


def check_confirmed(func):
    @wraps(func)
    def decorated_function(*args, **kwargs):
        if User.current_user().confirmed is False:
            flash('Please confirm your account!', 'warning')
            return redirect(url_for('users.unconfirmed'))
        return func(*args, **kwargs)

    return decorated_function

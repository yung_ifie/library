import requests
from flask_mail import Message
from src import app
from .decorators import async


@async
def send_async_email(my_app, msg):
    with my_app.app_context():
        app.mail.send(msg)


def send_email(to, subject, template):
    msg = Message(
        subject,
        recipients=[to],
        html=template,
        sender=app.app.config['MAIL_DEFAULT_SENDER']
    )
    send_async_email(app.app, msg)


def send_email_with_mailgun(to, subject, template):
    request = requests.post(
        app.app.config['MAILGUN_URL'],
        auth=("api", app.app.config["MAILGUN_API_KEY"]),
        data={"from": app.app.config['MAILGUN_FROM'], "to": to, "subject": subject,
              "text": template,
              "html": template}
    )

    print('Status: {0}'.format(request.status_code))
    print('Body:   {0}'.format(request.text))

from flask import Flask, render_template, session, redirect, url_for
from flask_mail import Mail
import src.scripts.set_environment_variables as set_environment_variables
import src.models.users.decorators as user_decorators
from src.common.database import Database
from src.models.users.user import User
from src.models.users.views import user_blueprint
from src.models.uploaders.views import upload_blueprint
from src.models.folders.views import folder_blueprint

app = Flask(__name__)
set_environment_variables
app.config.from_object('src.config')
app.secret_key = app.config['SECRET_KEY']
mail = Mail(app)


@app.before_first_request
def init_db():
    Database.initialize()


@app.route('/')
@user_decorators.requires_login
def home():
    user = User.find_by_email(session['email'])

    return redirect(url_for('folders.folders'))
    # return render_template('home.jinja2', user=user)


app.register_blueprint(user_blueprint, url_prefix="/users")
app.register_blueprint(upload_blueprint, url_prefix="/uploads")
app.register_blueprint(folder_blueprint, url_prefix="/folders")

from src.common.database import Database
from src.models.uploaders.uploader import Uploader

Database.initialize()

Uploader.update_new_tag()

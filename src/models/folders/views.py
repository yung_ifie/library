from collections import defaultdict
from flask import Blueprint, request, render_template, session, redirect, url_for, flash
import src.models.users.decorators as user_decorators
from src.common.decorators import check_confirmed
from src.models.uploaders.uploader import Uploader

folder_blueprint = Blueprint('folders', __name__)


@folder_blueprint.route('/', methods=['GET', 'POST'])
@user_decorators.requires_login
@check_confirmed
def folders():
    root = Uploader.create_root(session['user_id'])

    if request.method == 'POST':
        folder_name = request.form['folder_name']
        parent = request.form['parent']

        ancestor = Uploader.find_ancestor(parent, session['user_id'])

        folder = Uploader(
            user_id=session['user_id'],
            upload_type='folder',
            file_name=folder_name,
            parent=parent,
        )

        folder.ancestors = ancestor.ancestors
        folder.ancestors.append(ancestor.file_name)

        folder.save_to_mongo()

        flash("Folder created successfully", 'success')
        return redirect(url_for('.folder_list_for_id', parent_id=folder.get_id))

    return redirect(url_for('.folder_list_for_ancestor', parent_name=root.file_name))


@folder_blueprint.route('/<string:parent_id>')
@user_decorators.requires_login
@check_confirmed
def folder_list_for_id(parent_id):
    user_files = Uploader.all(session['user_id'], parent_id)
    ancestor = Uploader.find_ancestor(parent_id, session['user_id'])
    quick_folder = Uploader.quick_folder_access(session['user_id'])

    files_map = defaultdict(list)

    for user_file in user_files:
        files_map[user_file.upload_type].append(user_file)

    return render_template('uploaders/index.jinja2',
                           files=files_map,
                           parent_id=parent_id,
                           routes=ancestor.ancestors,
                           present_folder=ancestor.file_name,
                           quick_folder=quick_folder)


@folder_blueprint.route('/<string:parent_name>/ancestor')
@user_decorators.requires_login
@check_confirmed
def folder_list_for_ancestor(parent_name):
    ancestor = Uploader.find_ancestor_by_name(parent_name, session['user_id'])
    user_files = Uploader.all(session['user_id'], ancestor.get_id)
    quick_folder = Uploader.quick_folder_access(session['user_id'])

    files_map = defaultdict(list)

    for user_file in user_files:
        files_map[user_file.upload_type].append(user_file)

    return render_template('uploaders/index.jinja2',
                           files=files_map,
                           parent_id=ancestor.get_id,
                           routes=ancestor.ancestors,
                           present_folder=ancestor.file_name,
                           quick_folder=quick_folder)


@folder_blueprint.route('/<string:parent_id>/search')
@user_decorators.requires_login
@check_confirmed
def folder_search(parent_id):
    query = request.args['query']
    user_files = Uploader.search(session['user_id'], parent_id, query)
    ancestor = Uploader.find_ancestor(parent_id, session['user_id'])
    quick_folder = Uploader.quick_folder_access(session['user_id'])

    files_map = defaultdict(list)

    for user_file in user_files:
        files_map[user_file.upload_type].append(user_file)

    return render_template('uploaders/index.jinja2',
                           files=files_map,
                           query=query,
                           parent_id=ancestor.get_id,
                           routes=ancestor.ancestors,
                           present_folder=ancestor.file_name,
                           quick_folder=quick_folder)


@folder_blueprint.route('/<string:file_id>/delete', methods=['POST'])
@user_decorators.requires_login
@check_confirmed
def delete(file_id):
    upload_type = Uploader.remove_file(file_id, session['user_id'])

    if upload_type == 'folder':
        flash("Folder successfully deleted", 'success')
    else:
        flash("File successfully deleted", 'success')

    return redirect(request.referrer)

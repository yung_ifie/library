import src.models.users.errors as UserErrors


class Validator(object):

    @staticmethod
    def validate_user_credentials(email, password):
        if email is None:
            raise UserErrors.InvalidError("Email cannot be empty.")

        if password is None:
            raise UserErrors.InvalidError("Password cannot be empty.")

        return True

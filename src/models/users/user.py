import uuid

import datetime
from flask import session

from src.common.database import Database
import src.models.users.errors as UserErrors
import src.models.users.constants as UserConstants
from src.common.utils import Utils


class User(object):
    def __init__(self, email, password, confirmed=False, registered_on=None, confirmed_on=None, _id=None):
        self.email = email
        self.password = password
        self.registered_on = datetime.datetime.utcnow() if registered_on is None else registered_on
        self.confirmed = confirmed
        self.confirmed_on = confirmed_on
        self._id = uuid.uuid4().hex if _id is None else _id

    def __repr__(self):
        return "<User {}>".format(self.email)

    @staticmethod
    def login_succeed(email, password):
        """
        This method validates the originality of the user email & password
        :param email: The users email
        :param password: The sha512 hashed password
        :return: True if valid, False if otherwise
        """

        user = Database.find_one('users', {'email': email})

        if user is None:
            raise UserErrors.UserNotExistsError("This user does not exist")

        if not Utils.check_hashed_password(password, user['password']):
            raise UserErrors.IncorrectPasswordError("Your password was wrong.")

        User.login(User(email=user['email'], password=user['password'], _id=user['_id']))

        return True

    @staticmethod
    def register_user(email, password):
        user_data = Database.find_one(UserConstants.COLLECTION, {"email": email})

        if user_data is not None:
            raise UserErrors.UserAlreadyRegisteredError("The email you used to register already exists.")

        if not Utils.email_is_valid(email):
            raise UserErrors.InvalidEmailError("The email does not have the right format.")

        user = User(email, Utils.hash_password(password))
        user.save_to_db()
        # User.login(user)

        return user

    @property
    def get_id(self):
        return self._id

    @classmethod
    def current_user(cls):
        user = None
        if session['email']:
            user = User.find_by_email(session['email'])

        if user is None:
            raise UserErrors.UserNotExistsError("This user does not exist")

        return user

    @staticmethod
    def login(user):
        session['email'] = user.email
        session['user_id'] = user.get_id

    @staticmethod
    def logout():
        session['email'] = None
        session['user_id'] = None

    def save_to_db(self):
        Database.update(UserConstants.COLLECTION, {'_id': self._id}, self.json())

    def json(self):
        return {
            "_id": self._id,
            "email": self.email,
            "password": self.password,
            "registered_on": self.registered_on,
            "confirmed": self.confirmed,
            "confirmed_on": self.confirmed_on
        }

    @classmethod
    def find_by_email(cls, email):
        return cls(**Database.find_one(UserConstants.COLLECTION, {'email': email}))

from functools import wraps

from flask import session, redirect, url_for, request


def requires_login(func):
    @wraps(func)
    def decorated_function(*args, **kwargs):
        if 'email' not in session.keys() or session['email'] is None:
            return redirect(url_for('users.login_user', next=request.path))
        return func(*args, **kwargs)  # func(...) args: func(5, 6) kwargs: func(x=5, y=6)
    return decorated_function

import datetime
from flask import Blueprint, request, url_for, render_template, flash, session
from werkzeug.utils import redirect
import src.models.users.errors as UserErrors
import src.models.users.decorators as user_decorators
from src.common.decorators import check_confirmed
from src.common.email import send_email, send_email_with_mailgun
from src.models.uploaders.uploader import Uploader
from src.models.users.token import generate_confirmation_token, confirm_token
from src.models.users.user import User

user_blueprint = Blueprint('users', __name__)


@user_blueprint.route('/login', methods=['GET', 'POST'])
def login_user():
    if request.method == 'POST':
        email = request.form['email']
        password = request.form['password']

        try:
            if User.login_succeed(email, password):
                flash('You were successfully logged in', 'success')
                return redirect(url_for('folders.folders'))

        except UserErrors.UserError as e:
            return render_template("users/login.jinja2",
                                   error=e.message,
                                   title='Login')

    return render_template("users/login.jinja2", title='Login')   # Send the user an error if login is invalid


@user_blueprint.route('/register', methods=['GET', 'POST'])
def register_user():
    if request.method == 'POST':
        email = request.form['email']
        password = request.form['password']

        try:
            user = User.register_user(email, password)
            if user:
                Uploader.create_root(user.get_id)
                token = generate_confirmation_token(user.email)

                confirm_url = url_for('users.confirm_email', token=token, _external=True)
                html = render_template('users/activate.html', confirm_url=confirm_url)

                subject = "Please confirm your email"

                send_email(user.email, subject, html)

                flash('A confirmation email has been sent via email.', 'success')
                # return redirect(url_for(".login_user"))
                return redirect(url_for(".unconfirmed"))

        except UserErrors.UserError as e:
            return render_template("users/register.jinja2",
                                   error=e.message,
                                   title='Register')

    return render_template("users/register.jinja2", title='Register')   # Send the user an error if login is invalid


@user_blueprint.route('/confirm/<token>')
def confirm_email(token):
    try:
        email = confirm_token(token)
    except:
        flash('The confirmation link is invalid or has expired.', 'danger')

    user = User.find_by_email(email)

    if user.confirmed:
        flash('Account already confirmed. Please login.', 'success')
        return redirect(url_for('.login_user'))
    else:
        user.confirmed = True
        user.confirmed_on = datetime.datetime.now()
        user.save_to_db()

        User.login(user)

        flash('You have confirmed your account. Thanks!', 'success')
    return redirect(url_for('folders.folders'))


@user_blueprint.route('/<string:user_id>/uploads')
@user_decorators.requires_login
@check_confirmed
def user_uploads(user_id):
    user_files = Uploader.all(user_id)

    return render_template('uploaders/index.jinja2', files=user_files, title='Uploads')


@user_blueprint.route('/unconfirmed')
@user_decorators.requires_login
def unconfirmed():
    if User.current_user().confirmed:
        return redirect('folders.folders')

    # flash('Please confirm your account!', 'warning')
    return render_template('users/unconfirmed.jinja2')


@user_blueprint.route('/resend')
@user_decorators.requires_login
def resend_confirmation():
    current_user = User.current_user()

    token = generate_confirmation_token(current_user.email)
    confirm_url = url_for('.confirm_email', token=token, _external=True)
    html = render_template('users/activate.html', confirm_url=confirm_url)
    subject = "Please confirm your email"
    send_email(current_user.email, subject, html)

    flash('A new confirmation email has been sent.', 'success')
    return redirect(url_for('users.unconfirmed'))


@user_blueprint.route('/logout')
def logout_user():
    User.logout()

    return redirect(url_for('home'))  # .home for redirecting in current view

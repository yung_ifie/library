import os
from flask import Blueprint, request, render_template, session, send_from_directory, redirect, url_for, flash
from werkzeug.utils import secure_filename
import src.models.users.decorators as user_decorators
from src.definitions import ROOT_PATH
from src.models.uploaders.uploader import Uploader
from src.models.users.user import User

upload_blueprint = Blueprint('uploader', __name__)

APP_ROOT = ROOT_PATH


@upload_blueprint.route('/new/<string:parent_id>', methods=['GET', 'POST'])
@user_decorators.requires_login
def upload(parent_id):
    user = User.find_by_email(session['email'])
    target = os.path.join(APP_ROOT, 'public/uploads/{}'.format(user.get_id))
    quick_folder = Uploader.quick_folder_access(session['user_id'])

    if not os.path.isdir(target):
        os.mkdir(target)

    if request.files.getlist("file_upload"):
        for file in request.files.getlist("file_upload"):
            if file and Uploader.allowed_file(file.filename):
                file_name = secure_filename(file.filename).lower()
                destination = "/".join([target, file_name])
                file.save(destination)

                doc = Uploader(
                    user_id=user.get_id,
                    file_name=file_name,
                    ext_name=Uploader.ext_name(file_name),
                    parent=parent_id
                )

                doc.save_to_mongo()

            else:
                flash('File name: {} with extension {} is prohibited.'.format(file.filename,
                                                                              Uploader.ext_name(file.filename)),
                      'warning')
                return redirect(request.referrer)

        flash('File successfully uploaded', 'success')

        return redirect(url_for('folders.folder_list_for_id', parent_id=parent_id))

    return render_template('uploaders/upload.jinja2', parent_id=parent_id, quick_folder=quick_folder)


@upload_blueprint.route('/<string:user_id>/view/<string:file_id>')
def send_file(user_id, file_id):
    file = Uploader.find_one(file_id, user_id)
    return send_from_directory(Uploader.path(user_id), file.file_name)


@upload_blueprint.route('/<string:user_id>/<string:file_id>/download')
def download_file(user_id, file_id):
    file = Uploader.find_one(file_id, user_id)
    return send_from_directory(Uploader.path(user_id),
                               file.file_name,
                               as_attachment=True)

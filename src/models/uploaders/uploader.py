import uuid
from os.path import splitext
import datetime
import os
from werkzeug.utils import secure_filename
from src.common.database import Database
import src.models.uploaders.constants as UploadConstants
from src.definitions import ROOT_PATH


class Uploader(object):
    ALLOWED_EXTENSIONS = set(['dotx', 'docx', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])

    def __init__(self, user_id, upload_type='file', file_name=None, ext_name=None, created_at=None, new=None,
                 ancestors=[], parent=None, _id=None):

        self.file_name = file_name.lower() if file_name is not None else file_name
        self.ext_name = ext_name.lower() if ext_name is not None else ext_name
        self.upload_type = upload_type
        self.created_at = datetime.datetime.utcnow() if created_at is None else created_at
        self.new = True if new is None else new
        self.ancestors = ancestors
        self.parent = parent if parent is not None else parent
        self.user_id = user_id
        self._id = uuid.uuid4().hex if _id is None else _id

    def json(self):
        return {
            "file_name": self.file_name,
            "ext_name": self.ext_name,
            "upload_type": self.upload_type,
            "created_at": self.created_at,
            "new": self.new,
            "ancestors": self.ancestors,
            "parent": self.parent,
            "user_id": self.user_id,
            "_id": self._id
        }

    @staticmethod
    def ext_name(path):
        return splitext(path)[-1]

    @classmethod
    def all(cls, user_id, parent_id):
        return [cls(**file) for file in Database.find(UploadConstants.COLLECTION,
                                                      {'user_id': user_id, 'parent': parent_id})]

    @classmethod
    def search(cls, user_id, parent_id, query):
        return [cls(**file) for file in Database.find(UploadConstants.COLLECTION,
                                                      {'user_id': user_id,
                                                       'parent': parent_id,
                                                       'file_name': {'$regex': query,
                                                                     '$options': 'imx'}})]

    @staticmethod
    def find_file(file_name, parent_id, user_id):
        return Database.find_one(UploadConstants.COLLECTION,
                                 {'file_name': file_name,
                                  'parent': parent_id,
                                  'user_id': user_id})

    @classmethod
    def find_one(cls, file_id, user_id):
        return cls(**Database.find_one(UploadConstants.COLLECTION,
                                       {'_id': file_id,
                                        'user_id': user_id}))

    @classmethod
    def find_ancestor(cls, parent_id, user_id):
        return cls(**Database.find_one(UploadConstants.COLLECTION,
                                       {'_id': parent_id,
                                        'upload_type': 'folder',
                                        'user_id': user_id}))

    @property
    def get_id(self):
        return self._id

    @classmethod
    def find_ancestor_by_name(cls, parent_name, user_id):
        return cls(**Database.find_one(UploadConstants.COLLECTION,
                                       {'file_name': parent_name,
                                        'upload_type': 'folder',
                                        'user_id': user_id}))

    def save_to_mongo(self):
        if Uploader.find_file(self.file_name, self.parent, self.user_id) is None:
            Database.save(UploadConstants.COLLECTION, {'_id': self._id}, self.json())

    @staticmethod
    def remove_file(file_id, user_id):
        file = Database.find_one(UploadConstants.COLLECTION, {'_id': file_id, 'user_id': user_id})

        Database.remove_many(UploadConstants.COLLECTION, {'_id': file_id, 'user_id': user_id})

        if file['upload_type'] == 'folder':
            Database.remove_many(UploadConstants.COLLECTION, {'parent': file_id, 'user_id': user_id})

            Database.update(UploadConstants.COLLECTION, {'user_id': user_id},
                            {'$pull': {'ancestors': {'$in': [file['file_name']]}}})
        else:
            target = Uploader.path(user_id)
            file_name = secure_filename(file['file_name'])
            destination = "/".join([target, file_name])
            os.remove(destination)

        return file['upload_type']

    @classmethod
    def create_root(cls, user_id):
        seed = Uploader(
            file_name='root',
            upload_type='folder',
            user_id=user_id
        )

        root = Database.find_one(UploadConstants.COLLECTION, {'file_name': seed.file_name, 'user_id': user_id})

        if root is None:
            Database.insert(UploadConstants.COLLECTION, seed.json())
            return seed
        else:
            return cls(**root)

    @classmethod
    def quick_folder_access(cls, user_id):
        return [cls(**file) for file in
                Database.find(UploadConstants.COLLECTION, {'user_id': user_id, 'upload_type': 'folder'}).limit(10)]

    @classmethod
    def find(cls, query):
        return [cls(**file) for file in
                Database.find(UploadConstants.COLLECTION, query)]

    def save(self):
        Database.save(UploadConstants.COLLECTION, {'_id': self._id}, self.json())

    @staticmethod
    def update_new_tag():
        for file in Uploader.find({'new': True}):
            exp_date = file.created_at + datetime.timedelta(days=1)

            print("Exp Date: {}".format(exp_date))
            print("Created Date: {}".format(file.created_at))
            if datetime.datetime.utcnow() > exp_date:
                file.new = False
                file.save()

    @staticmethod
    def allowed_file(filename):
        return '.' in filename and \
               filename.rsplit('.', 1)[1].lower() in Uploader.ALLOWED_EXTENSIONS

    @staticmethod
    def path(user_id):
        return os.path.join(ROOT_PATH, 'public/uploads/{}'.format(user_id))
